package com.xee.course.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.xee.course.Activities.SupportActivities.FragmentNavigationActivity;
import com.xee.course.Fragments.AboutUsFragment;
import com.xee.course.Fragments.ForumFragment;
import com.xee.course.Fragments.MainFragment;
import com.xee.course.Fragments.StoriesListFragment.StoriesListFragment;
import com.xee.course.Fragments.StudyingFragment.StudyingFragment;
import com.xee.course.R;


/**
 * Main Activity
 */
public class MainActivity extends FragmentNavigationActivity {
    public final String TAG = "MainActivityTag";
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setFragment();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.main_menu_stories:
                        showFragment(StoriesListFragment.TAG);
                        return true;

                    case R.id.main_menu_studying:
                        showFragment(StudyingFragment.TAG);
                        return true;

                    case R.id.main_menu_way:
                        showFragment(MainFragment.TAG);
                        return true;

                    case R.id.main_menu_forum:
                        showFragment(ForumFragment.TAG);
                        return true;

                    case R.id.main_menu_aboutus:
                        showFragment(AboutUsFragment.TAG);
                        return true;
                }
                return false;
            }
        });
    }
}
