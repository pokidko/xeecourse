package com.xee.course.Activities.SupportActivities;

import android.support.v7.app.AppCompatActivity;

import com.xee.course.Utils.Eventbus.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

abstract public class BaseActivity extends AppCompatActivity {


    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {

    }
}
