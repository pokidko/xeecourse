package com.xee.course.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.LessonData;
import com.xee.course.Utils.Database.DBHandler;

public class PhotoViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_photo_view);

        int lessonId = getIntent().getIntExtra(Constants.KEY_LESSON_ID, 1);
        final LessonData lessonData = DBHandler.getInstance(this).getLessonDataByLessonId(lessonId);

        getSupportActionBar().hide();

        //setting photo
        final ImageView photoHolder = (ImageView) findViewById(R.id.activity_photo_holder);

        //setting image to holder
        photoHolder.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    // Wait until layout to call Picasso
                    @Override
                    public void onGlobalLayout() {
                        // Ensure we call this only once
                        photoHolder.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);

                        Picasso.with(getApplicationContext())
                                .load(lessonData.getFilePasPhoto())
                                .resize(photoHolder.getMeasuredWidth(), photoHolder.getMeasuredHeight())
                                .centerCrop()
                                .into(photoHolder);
                    }
                });
    }
}
