package com.xee.course.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.xee.course.R;

public class EvangilieDisplayingActivity extends AppCompatActivity {

    public static final String evangilieTextFilePath = "file:///android_asset/evangilie.htm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evangilie_displaying);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        WebView webView = (WebView) findViewById(R.id.evangilie_web_view);
        webView.loadUrl(evangilieTextFilePath);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
    }
}
