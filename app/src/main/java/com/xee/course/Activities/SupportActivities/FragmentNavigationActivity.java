package com.xee.course.Activities.SupportActivities;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.xee.course.Fragments.AboutUsFragment;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.ForumFragment;
import com.xee.course.Fragments.FullEEFragment.FullEEFragment;
import com.xee.course.Fragments.LessonFragment.LessonFragment;
import com.xee.course.Fragments.LessonPlanFragment.LessonPlanFragment;
import com.xee.course.Fragments.LessonsListFragment.LessonsListFragment;
import com.xee.course.Fragments.MainFragment;
import com.xee.course.Fragments.NoteFragment.NoteFragment;
import com.xee.course.Fragments.NotesListFragment.NotesListFragment;
import com.xee.course.Fragments.RdnFragment.RdnFragment;
import com.xee.course.Fragments.StoriesListFragment.StoriesListFragment;
import com.xee.course.Fragments.StoryFragment.StoryFragment;
import com.xee.course.Fragments.StudyingFragment.StudyingFragment;
import com.xee.course.Fragments.WhatsNextFragment.WhatsNextFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.SavedFragmentData;

abstract public class FragmentNavigationActivity extends BaseActivity {
    public static final String TAG = "fragmNavTag";

    //flags that define fragment contener to hold fragment
    public static final int FLAG_MAIN_ACTIVITY_CONTAINER = 1;
    public static final int FLAG_STORY_AUDIO_PLAYER_CONTAINER = 2;
    //bottom navigation bar
    protected BottomNavigationView bottomNavigationView;
    //current fragment in main activity container
    BaseFragment fragment;
    //flag to manage onBackPressed exit from app
    private boolean toClose = false;

    /**
     * Show fragment with specified tag in MAIN_ACTIVITY_CONTAINER
     */
    public void showFragment(String fragmentTag) {
        showFragment(fragmentTag, null);
    }

    /**
     * Show fragment with specified tag in MAIN_ACTIVITY_CONTAINER
     * with bundle
     */
    public void showFragment(String fragmentTag, Bundle bundle) {
        showFragment(fragmentTag, bundle, FLAG_MAIN_ACTIVITY_CONTAINER);
    }

    /**
     * Show fragment with specified tag in specified container
     */
    public void showFragment(String fragmentTag, Bundle bundle, int containerFlag) {
        //check if containerFlag is legal
        if (!(containerFlag == FLAG_MAIN_ACTIVITY_CONTAINER
                || containerFlag == FLAG_STORY_AUDIO_PLAYER_CONTAINER)) {
            throw new IllegalArgumentException("wrong containerFlag passed");
        }

        if (containerFlag == FLAG_MAIN_ACTIVITY_CONTAINER) {
            saveFragmentDataToPref(fragmentTag, bundle);
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (fragment != null && (fragmentTag.equals(NotesListFragment.TAG)
                || fragmentTag.equals(NoteFragment.TAG))) {
            fragmentTransaction.addToBackStack(fragment.getTag());
        }


        BaseFragment newFragment = null;

        switch (fragmentTag) {
            case StoryFragment.TAG:
                fragment = new StoryFragment();
                fragment.setArguments(bundle);
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragmentTransaction.addToBackStack(null);
                break;

            case StoriesListFragment.TAG:
                hideAppBar();
                fragment = new StoriesListFragment();
                fragmentTransaction.addToBackStack(null);
                break;

            case StudyingFragment.TAG:
//                hideAppBar();
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                fragment = new StudyingFragment();
                fragmentTransaction.addToBackStack(null);
                break;

            case RdnFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new RdnFragment();
                break;

            case FullEEFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new FullEEFragment();
                break;

            case LessonsListFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new LessonsListFragment();
                if (bundle != null) {
                    fragment.setArguments(bundle);
                }
                fragmentTransaction.addToBackStack(null);
                break;

            case LessonFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new LessonFragment();
                if (bundle != null) {
                    fragment.setArguments(bundle);
                }
                fragmentTransaction.addToBackStack(null);
                break;

            case WhatsNextFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new WhatsNextFragment();
                if (bundle != null) {
                    //expect lesson id in bundle to show  lesson plan related to lesson
                    fragment.setArguments(bundle);
                }
                fragmentTransaction.addToBackStack(null);
                break;

            case LessonPlanFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new LessonPlanFragment();
                if (bundle != null) {
                    //expect lesson id in bundle
                    fragment.setArguments(bundle);
                }
                fragmentTransaction.addToBackStack(null);
                break;

            case NotesListFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new NotesListFragment();
                if (bundle != null) {
                    //expect lesson id in bundle to show note related to it
                    fragment.setArguments(bundle);
                }
//                fragmentTransaction.addToBackStack(fragmentTag);
                fragmentTransaction.addToBackStack(null);
                break;

            case NoteFragment.TAG:
                showAppBar();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                fragment = new NoteFragment();
                if (bundle != null) {
                    fragment.setArguments(bundle);
                }
//                fragmentTransaction.addToBackStack(fragmentTag);
                fragmentTransaction.addToBackStack(null);
                break;


            case MainFragment.TAG:
                hideAppBar();
                fragment = new MainFragment();
                fragmentTransaction.addToBackStack(null);
                break;

            case ForumFragment.TAG:
                hideAppBar();
                fragment = new ForumFragment();
                fragmentTransaction.addToBackStack(null);
                break;

            case AboutUsFragment.TAG:
                hideAppBar();
                fragment = new AboutUsFragment();
                fragmentTransaction.addToBackStack(null);
                break;

        }

        switch (containerFlag) {
            case FLAG_MAIN_ACTIVITY_CONTAINER:
                fragmentTransaction.replace(R.id.main_activity_container, fragment);
                break;

            case FLAG_STORY_AUDIO_PLAYER_CONTAINER:
                if (newFragment == null) {
                    throw new IllegalStateException("trying to put null-fragment into container");
                }
//                fragmentTransaction.add(R.id.story_fragment_player_holder, newFragment);
                break;
        }

        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        //realizing closing app only after second time back button pressed
        if (getSupportFragmentManager().getBackStackEntryCount() < 1) {
            if (toClose) {
                toClose = false;
                super.onBackPressed();
            } else {
                //show message
                Toast.makeText(this, getString(R.string.back_pressed_on_exit_text), Toast.LENGTH_SHORT).show();
                //set bool as true, to exit
                toClose = true;

                //wait some time and set bool as false
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toClose = false;
                    }
                }, 3000);
            }
        } else {
//            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
//                showFragment(MainFragment.TAG);
//                return;
//            }

            super.onBackPressed();
        }
    }


    /**
     * Showing fragment (default or saved in pref)
     * and set checked bottom menu item
     */
    protected void setFragment() {
        boolean setMainFragment = getIntent().getBooleanExtra(Constants.KEY_START_MAIN_FRAGMENT, false);
        if (setMainFragment) {
            setBottomMenuItemChecked(MainFragment.TAG);
            showFragment(MainFragment.TAG);

            getIntent().removeExtra(Constants.KEY_START_MAIN_FRAGMENT);
        } else {
            if (containsSavedFragment()) {
                SavedFragmentData savedFragmentData = getSavedFragmentFromPref();
                //do not load forum fragment
                if (savedFragmentData.getFragmentTag().equals(ForumFragment.TAG)) {
                    return;
                }
                setBottomMenuItemChecked(savedFragmentData.getFragmentTag());
                showFragment(savedFragmentData.getFragmentTag(), savedFragmentData.getBundle());
            } else {
                setBottomMenuItemChecked(MainFragment.TAG);
                showFragment(MainFragment.TAG);
            }
        }
    }

    /**
     * Setting checked bottom menu item depending on fragment tag
     */
    public void setBottomMenuItemChecked(String fragmentTag) {
        final int STORIES = 0;
        final int STUDYING = 1;
        final int WAY = 2;
        final int FORUM = 3;
        final int ABOUT_US = 4;
        switch (fragmentTag) {
            case StoryFragment.TAG:
                bottomNavigationView.getMenu().getItem(STORIES).setChecked(true);
                break;

            case StoriesListFragment.TAG:
                bottomNavigationView.getMenu().getItem(STORIES).setChecked(true);
                break;

            case StudyingFragment.TAG:
                bottomNavigationView.getMenu().getItem(STUDYING).setChecked(true);
                break;

            case MainFragment.TAG:
                bottomNavigationView.getMenu().getItem(WAY).setChecked(true);
                break;

            case ForumFragment.TAG:
                bottomNavigationView.getMenu().getItem(FORUM).setChecked(true);
                break;

            case AboutUsFragment.TAG:
                bottomNavigationView.getMenu().getItem(ABOUT_US).setChecked(true);
                break;

            default:
                bottomNavigationView.getMenu().getItem(STUDYING).setChecked(true);
                break;
        }
    }

    public void hideAppBar() {
        getSupportActionBar().hide();
    }

    public void showAppBar() {
        getSupportActionBar().show();
    }


    public void saveFragmentDataToPref(String fragmentTag, Bundle bundle) {
        Log.d(TAG, "saveFragmentDataToPref: " + fragmentTag);
        if (bundle != null) {
            Log.d(TAG, "saveFragmentDataToPref: " + bundle.toString());
        } else {
            Log.d(TAG, "saveFragmentDataToPref: null");
        }

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.KEY_SAVED_FRAGMENT, new SavedFragmentData(fragmentTag, bundle).toJson());
        editor.apply();
    }

    public SavedFragmentData getSavedFragmentFromPref() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        return SavedFragmentData.getDataFromJson(preferences.getString(Constants.KEY_SAVED_FRAGMENT, ""));
    }

    public boolean containsSavedFragment() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        return preferences.contains(Constants.KEY_SAVED_FRAGMENT);
    }
}
