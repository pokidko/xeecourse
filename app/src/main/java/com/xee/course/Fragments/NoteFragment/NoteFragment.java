package com.xee.course.Fragments.NoteFragment;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.ConfirmDeletionDialogFragment;
import com.xee.course.Fragments.StudyingFragment.StudyingFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.Note;
import com.xee.course.Utils.Database.DBHandler;
import com.xee.course.Utils.Eventbus.MessageEvent;
import com.xee.course.Utils.Utils;

/**
 * Fragment for creation and editing Notes
 */
public class NoteFragment extends BaseFragment {

    // 1. get note or create new one
    // 2. get data from note from p.1 and put data to fields
    // 3. onPause: save noteID to sharedPref and note to DB
    // 4. onResume: get note from DB by id stored in shered pref
    // 5. on save button pressed: save note, make preview mode
    // 6. on delete button pressed: delete note from db, delete note id from sher pref, close fragment
    // 7. on edit button pressed:  make edit mode
    // 8. onBackPressed: if editing is in progress - save note to DB, back to note preview; else - close fragment

    //TAGs
    public static final String TAG = "noteFragmentTag";
    public static final String TAG_NOTE_FRAGMENT_EDIT = "noteEditFragmentTag";
    public static final String TAG_NOTE_FRAGMENT_PREVIEW = "notePrevewFragmentTag";

    MenuItem menuEditNote;
    MenuItem menuSaveNote;
    MenuItem menuDeleteNote;
    private EditText titleNote;
    private EditText textNote;

    private boolean editNote;

    private Note note;

    public NoteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_note, container, false);

        setHasOptionsMenu(true);

        //get text fields
        titleNote = (EditText) rootView.findViewById(R.id.note_edit_title);
        textNote = (EditText) rootView.findViewById(R.id.note_edit_text);

        //if arguments contain note ID to edit set editNote = true
        if (getArguments() != null && getArguments().containsKey(Constants.KEY_EDIT_NOTE_ID)) {
            //preview old one
            setFieldsEditable(false);

            int noteId = getArguments().getInt(Constants.KEY_EDIT_NOTE_ID);
            note = DBHandler.getInstance(getContext()).getNoteById(noteId);

            if (note == null) {
//                note = new Note("magic", "we need some magic");
                throw new IllegalStateException("note couldn't be null here");
            }
        } else {
            //creating new note
            note = new Note("");
            //setting id of note
            note.setId(DBHandler.getInstance(getContext()).addNoteToDB(note));

            if (getArguments() != null && getArguments().containsKey(Constants.KEY_LESSON_ID)) {
                //setting id of lesson note is related to
                note.setLessonId(getArguments().getInt(Constants.KEY_LESSON_ID));
            } else {
                note.setLessonId(Note.NO_LESSON_NOTE);
            }

            setFieldsEditable(true);

            showKeyboardToEditNote();
        }

        //fill fields with note data
        fillDataFields(rootView, note);

        return rootView;
    }

    /**
     * Showing keyboard to edit title of note
     */
    private void showKeyboardToEditNote() {
//        textNote.requestFocus();
//        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(textNote, InputMethodManager.SHOW_IMPLICIT);
        textNote.post(new Runnable() {
            public void run() {
                textNote.requestFocusFromTouch();
                InputMethodManager lManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                lManager.showSoftInput(textNote, 0);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_note_fragment, menu);

        menuEditNote = menu.findItem(R.id.edit_note);
        menuSaveNote = menu.findItem(R.id.save_note);
        menuDeleteNote = menu.findItem(R.id.delete_note);

        updateMenuItems();
//        Utils.showMenuItems(menuSaveNote, menuEditNote, menuDeleteNote);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                if (editNote) {
                    note = getNoteFromFields();
                    saveNoteToDB(note);

                    Utils.hideKeyboard(getActivity());

                    if (note.getTextOfNote().isEmpty()) {
                        deleteNote();
                    }
//                    setFieldsEditable(false);

                } else {
                    deleteNotesFromPref();
                }

                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getActivity().onBackPressed();
                } else {
                    ((MainActivity) getActivity()).showFragment(StudyingFragment.TAG);
                }
                return true;

            case R.id.save_note:
                // saving note
                // backing to preview mode
                note = getNoteFromFields();
                saveNoteToDB(note);
                saveNoteToPref(note);
                setFieldsEditable(false);

                Utils.hideKeyboard(getActivity());

                updateMenuItems();
                break;

            case R.id.edit_note:
                setFieldsEditable(true);

                updateMenuItems();
                showKeyboardToEditNote();
                break;

            case R.id.delete_note:
                (new ConfirmDeletionDialogFragment()).show(getFragmentManager(), "confirmDeletion");
                break;
        }
        return false;
    }

    @Override
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {
            case MENU_DELETION_CONFIRM:
                deleteNote();
                close();

                updateMenuItems();
                break;


//            case MENU_SAVE_PRESSED:
//                // saving note
//                // backing to preview mode
//                note = getNoteFromFields();
//                saveNoteToDB(note);
//                saveNoteToPref(note);
//                setFieldsEditable(false);
//
//                Utils.hideKeyboard(getActivity());
//
//                updateMenuItems();
//                break;
//
//            case MENU_EDIT_PRESSED:
//                setFieldsEditable(true);
//
//                updateMenuItems();
//                break;
//
//            case MENU_DELETE_PRESSED:
//                (new ConfirmDeletionDialogFragment()).show(getFragmentManager(), "confirmDeletion");
//                break;
        }
    }

    private void updateMenuItems() {
//        OldMainActivity activity = (OldMainActivity) getActivity();
//        activity.setMenuItems(getFragmentTag());

        String tag = getFragmentTag();
        Utils.hideMenuItems(menuDeleteNote, menuEditNote, menuSaveNote);

        switch (tag) {
            case TAG_NOTE_FRAGMENT_EDIT:
                Utils.showMenuItems(menuSaveNote, menuDeleteNote);
                break;

            case TAG_NOTE_FRAGMENT_PREVIEW:
                Utils.showMenuItems(menuEditNote, menuDeleteNote);
                break;
        }
    }

//    @Override
//    public boolean onBackPressed() {
//        if (editNote) {
//            setFieldsEditable(false);
//            saveNoteToDB(note);
//        } else {
//            deleteNotesFromPref();
//            return super.onBackPressed();
//        }
//        return false;
//    }


    /**
     * Setting ability to edit note depending on state of editNote variable
     */
    private void setFieldsEditable(boolean editable) {
        editNote = editable;
        if (editable) {
//            textNote.setInputType(TEXT_INPUT_TYPE);
//            titleNote.setInputType(TITLE_INPUT_TYPE);
            textNote.setEnabled(true);
            titleNote.setEnabled(true);
        } else {
            //blocking ability to edit
//            textNote.setInputType(InputType.TYPE_NULL);
//            titleNote.setInputType(InputType.TYPE_NULL);

            textNote.setEnabled(false);
            titleNote.setEnabled(false);
        }
    }

    /**
     * Fills all fields in edit note fragment from specified note data
     *
     * @param rootView
     * @param note     to get data from
     */
    private void fillDataFields(View rootView, Note note) {
        ((TextView) rootView.findViewById(R.id.note_edit_title)).setText(note.getTitleOfNote());
//        ((TextView) rootView.findViewById(R.id.note_edit_text)).setText(note.getTextOfNote().isEmpty() ? note.getTextOfNote() : null);
        ((TextView) rootView.findViewById(R.id.note_edit_text)).setText(note.getTextOfNote());
    }

    /**
     * Fills all fields in edit note fragment from specified note data
     *
     * @param note to get data from
     */
    private void fillDataFields(Note note) {
        ((TextView) getActivity().findViewById(R.id.note_edit_title)).setText(note.getTitleOfNote());
        ((TextView) getActivity().findViewById(R.id.note_edit_text)).setText(note.getTextOfNote().isEmpty() ? note.getTextOfNote() : null);
    }

    /**
     * Creating instance of Note class from data from fields
     *
     * @return new instance of Note
     */
    private Note getNoteFromFields() {
        String title = (String) ((TextView) getActivity().findViewById(R.id.note_edit_title)).getText().toString();
        String text = (String) ((TextView) getActivity().findViewById(R.id.note_edit_text)).getText().toString();
        note.setTitleOfNote(title);
        note.setTextOfNote(text);
        return note;
    }

    @Override
    public String getFragmentTag() {
        if (editNote) {
            return TAG_NOTE_FRAGMENT_EDIT;
        } else {
            return TAG_NOTE_FRAGMENT_PREVIEW;
        }
    }

    private void saveNoteToPref(Note note) {
        SharedPreferences sharedPreferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putInt(Constants.KEY_CURRENT_NOTE_ID, note.getId());
        ed.apply();
    }

    /**
     * Getting note from shared preferences
     *
     * @return Note instance, or NULL if there is no note
     */
    private Note getSavedNoteFromPref() {
        SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);

        if (preferences.contains(Constants.KEY_CURRENT_NOTE_ID)) {
            int noteId = preferences.getInt(Constants.KEY_CURRENT_NOTE_ID, 0);
            return DBHandler.getInstance(getContext()).getNoteById(noteId);
        } else {
            return null;
        }
    }

    private boolean areSharedPrefContainsNote() {
        SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
        return preferences.contains(Constants.KEY_CURRENT_NOTE_ID);
    }

    /**
     * Deleting note id from preferences
     */
    private void deleteNotesFromPref() {
        SharedPreferences sharedPreferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.remove(Constants.KEY_CURRENT_NOTE_ID);
        ed.apply();
    }

    /**
     * Deleting note from DB and pref
     */
    private void deleteNote() {
        DBHandler.getInstance(getContext()).deleteNote(note);
        deleteNotesFromPref();
    }

    /**
     * Saving note to DB and to shared preferences
     *
     * @param note to save
     */
    private void saveNoteToDB(Note note) {
        note = getNoteFromFields();

        DBHandler.getInstance(getContext()).updateNote(note);
    }

    /**
     * Closing fragment
     */
    private void close() {
//        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (areSharedPrefContainsNote()) {
            note = getSavedNoteFromPref();
            fillDataFields(note);
        }
    }

    @Override
    public void onPause() {
        note = getNoteFromFields();
        saveNoteToPref(note);
        saveNoteToDB(note);
        if (note.getTextOfNote().isEmpty()) {
            deleteNote();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        deleteNotesFromPref();
        super.onStop();
    }
}
