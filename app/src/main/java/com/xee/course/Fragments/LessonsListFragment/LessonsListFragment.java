package com.xee.course.Fragments.LessonsListFragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.NotesListFragment.NotesListFragment;
import com.xee.course.Fragments.StudyingFragment.StudyingFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.LessonData;
import com.xee.course.Utils.Database.DBHandler;

import java.util.ArrayList;

/**
 * Displaying all lessons list
 */
public class LessonsListFragment extends BaseFragment {
    public static final String TAG = "lessonsListFragmentTag";

    int partId;

    RecyclerView recyclerView;
    LessonsRVAdapter rvAdapter;
    RecyclerView.LayoutManager rvLayoutManager;


    public LessonsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_lessons_list, container, false);

        //getting id of part
        partId = getArguments().getInt(Constants.KEY_PARTS_ID);

        initRV(rootView);

        setHasOptionsMenu(true);

        return rootView;
    }

    private void initRV(View rootView) {
        ArrayList<LessonData> lessons = new ArrayList<>();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.lessons_recycle);
        rvLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(rvLayoutManager);
        rvAdapter = new LessonsRVAdapter(lessons, getContext());

        recyclerView.setAdapter(rvAdapter);

        //getting note from DB
        lessons = DBHandler.getInstance(getContext()).getLessonsListByPart(partId);

        rvAdapter.setData(lessons);
        rvAdapter.notifyDataSetChanged();
        TextView noLessonsTextView = (TextView) rootView.findViewById(R.id.no_lessons_in_list);

        //setting visibility of no lessons text view and RecycleView
        if (lessons.isEmpty()) {
            noLessonsTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            noLessonsTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_styding, menu);
        menu.findItem(R.id.menu_rdn).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((MainActivity) getActivity()).showFragment(StudyingFragment.TAG);
                return true;

            case R.id.menu_notes:
                ((MainActivity) getActivity()).showFragment(NotesListFragment.TAG);
                return true;
        }
        return false;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
