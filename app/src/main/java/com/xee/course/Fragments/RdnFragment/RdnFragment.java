package com.xee.course.Fragments.RdnFragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.StudyingFragment.StudyingFragment;
import com.xee.course.R;


public class RdnFragment extends BaseFragment {
    public static final String TAG = "rdnFragmentTag";

    public static final String rdnTextFilePath = "file:///android_asset/rdn.htm";

    public RdnFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_rdn, container, false);

        WebView webView = (WebView) rootView.findViewById(R.id.rdn_web_view);
        webView.loadUrl(rdnTextFilePath);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //todo: change to back fragment
                ((MainActivity) getActivity()).showFragment(StudyingFragment.TAG);
                return true;
        }
        return false;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
