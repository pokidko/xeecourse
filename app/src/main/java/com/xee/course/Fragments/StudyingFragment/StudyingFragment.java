package com.xee.course.Fragments.StudyingFragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.xee.course.Activities.EvangilieDisplayingActivity;
import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.FullEEFragment.FullEEFragment;
import com.xee.course.Fragments.LessonsListFragment.LessonsListFragment;
import com.xee.course.Fragments.NotesListFragment.NotesListFragment;
import com.xee.course.Fragments.RdnFragment.RdnFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;

/**
 * Main studying fragment
 */
public class StudyingFragment extends BaseFragment {
    public static final String TAG = "studyingFragmentTag";

    RelativeLayout subtitle1;
    RelativeLayout subtitle2;
    RelativeLayout subtitle3;

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle partId;
            switch (view.getId()) {
                case R.id.study_frag_subtitle_1_layout:
                    partId = new Bundle();
                    partId.putInt(Constants.KEY_PARTS_ID, Constants.INT_CONNECT_LIFE_PART);
                    ((MainActivity) getContext()).showFragment(LessonsListFragment.TAG, partId);
                    break;

                case R.id.study_frag_subtitle_2_layout:
                    partId = new Bundle();
                    partId.putInt(Constants.KEY_PARTS_ID, Constants.INT_SHARE_LIFE);
                    ((MainActivity) getContext()).showFragment(LessonsListFragment.TAG, partId);
                    break;

                case R.id.study_frag_subtitle_3_layout:
                    partId = new Bundle();
                    partId.putInt(Constants.KEY_PARTS_ID, Constants.INT_MULTIPLY_LIFE);
                    ((MainActivity) getContext()).showFragment(LessonsListFragment.TAG, partId);
                    break;
            }
        }
    };

    public StudyingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_studying, container, false);

        setHasOptionsMenu(true);

        subtitle1 = (RelativeLayout) rootView.findViewById(R.id.study_frag_subtitle_1_layout);
        subtitle2 = (RelativeLayout) rootView.findViewById(R.id.study_frag_subtitle_2_layout);
        subtitle3 = (RelativeLayout) rootView.findViewById(R.id.study_frag_subtitle_3_layout);

        subtitle1.setOnClickListener(clickListener);
        subtitle2.setOnClickListener(clickListener);
        subtitle3.setOnClickListener(clickListener);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_styding, menu);
        menu.findItem(R.id.menu_evangilie).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((MainActivity) getActivity()).showFragment(StudyingFragment.TAG);
                return true;

            case R.id.menu_rdn:
                ((MainActivity) getActivity()).showFragment(RdnFragment.TAG);
                return true;

            case R.id.menu_notes:
                ((MainActivity) getActivity()).showFragment(NotesListFragment.TAG);
                return true;

            case R.id.menu_full_ee:
                ((MainActivity) getActivity()).showFragment(FullEEFragment.TAG);
                return true;

            case R.id.menu_evangilie:
                Intent intent = new Intent(getActivity(), EvangilieDisplayingActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
