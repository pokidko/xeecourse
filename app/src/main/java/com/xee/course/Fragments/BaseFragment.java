package com.xee.course.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.xee.course.Utils.Eventbus.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Base fragment
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {
    /**
     * Setting listeners to specified views
     */
    protected void setThisListeners(View... views) {
        for (View v : views)
            v.setOnClickListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {

    }

    public abstract String getFragmentTag();

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
    }
}
