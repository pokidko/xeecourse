package com.xee.course.Fragments.StoriesListFragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.squareup.picasso.Picasso;
import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.StoryFragment.StoryFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.StoryData;

import java.util.List;

public class StoriesRecycleViewAdapter extends RecyclerView.Adapter<StoryViewHolder> {

    private List<StoryData> storiesList;
    private Context context;

    public StoriesRecycleViewAdapter(Context context, List<StoryData> storiesList) {
        this.storiesList = storiesList;
        //adding 2 empty stories to display empty photos
        storiesList.add(null);
        storiesList.add(null);

        this.context = context;
    }


    @Override
    public StoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.stories_card_view_list, null);
        return new StoryViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final StoryViewHolder holder, final int position) {
        final StoryData storyData = storiesList.get(position);

        //setting image to holder
        holder.photoImageView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    // Wait until layout to call Picasso
                    @Override
                    public void onGlobalLayout() {
                        // Ensure we call this only once
                        holder.photoImageView.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);
                        if (position <= 3) {
                            Picasso.with(context)
                                    .load(storyData.getPhotoLink())
                                    .resize(holder.photoImageView.getMeasuredWidth(), holder.photoImageView.getMeasuredHeight())
                                    .centerCrop()
                                    .into(holder.photoImageView);
                        } else {
//                            Picasso.with(context).load("file:///android_asset/DvpvklR.png").into(imageView2);
                            Picasso.with(context)
                                    .load(R.drawable.temp_x_for_story)
                                    .resize(holder.photoImageView.getMeasuredWidth(), holder.photoImageView.getMeasuredHeight())
                                    .centerCrop()
                                    .into(holder.photoImageView);
                        }

                    }
                });

        //setting listeners. only for first 4 images
        holder.holderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle storyBundle = new Bundle();
                if (position <= 3) {
                    storyBundle.putInt(Constants.KEY_STORY_ID, storyData.getId());
                    ((MainActivity) context).showFragment(StoryFragment.TAG, storyBundle, MainActivity.FLAG_MAIN_ACTIVITY_CONTAINER);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.storiesList.size();
    }
}