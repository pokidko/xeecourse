package com.xee.course.Fragments.NotesListFragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xee.course.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NoteRVHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.note_title)
    TextView noteTitle;

    @Bind(R.id.note_text)
    TextView noteText;

    @Bind(R.id.note_holder_layout)
    LinearLayout layout;

    public NoteRVHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
