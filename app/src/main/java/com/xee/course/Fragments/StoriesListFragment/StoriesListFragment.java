package com.xee.course.Fragments.StoriesListFragment;


import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.xee.course.Activities.SupportActivities.FragmentNavigationActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.R;
import com.xee.course.Utils.DataKeepingClasses.StoryData;
import com.xee.course.Utils.Database.DBHandler;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoriesListFragment extends BaseFragment {
    public static final String TAG = "storiesFrTag";
    private static final String STORIES_ALBOM_YOUTUBE_LINK = "https://www.youtube.com/playlist?list=PLKMLWE2Zbs3ohjwFBlg1uV5YIJ5vaWxaI";
    private static int COLUMNS_NUMBER;
    private GridLayoutManager gridLayoutManager;

    public StoriesListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stories, container, false);

        if (getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            COLUMNS_NUMBER = 3;
        } else {
            COLUMNS_NUMBER = 6;
        }

        gridLayoutManager = new GridLayoutManager(getContext(), COLUMNS_NUMBER);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.stories_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);

        ArrayList<StoryData> stories = DBHandler.getInstance(getContext()).getStoriesList();

        StoriesRecycleViewAdapter adapter = new StoriesRecycleViewAdapter(getContext(), stories);
        recyclerView.setAdapter(adapter);

        ((Button) rootView.findViewById(R.id.stories_list_button_id)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(STORIES_ALBOM_YOUTUBE_LINK)));
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((FragmentNavigationActivity) getContext()).setBottomMenuItemChecked(getFragmentTag());
        ((FragmentNavigationActivity) getContext()).hideAppBar();
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
