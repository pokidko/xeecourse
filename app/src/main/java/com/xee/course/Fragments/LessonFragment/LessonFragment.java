package com.xee.course.Fragments.LessonFragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.xee.course.Activities.MainActivity;
import com.xee.course.Activities.PhotoViewActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.LessonPlanFragment.LessonPlanFragment;
import com.xee.course.Fragments.NoteFragment.NoteFragment;
import com.xee.course.Fragments.NotesListFragment.NotesListFragment;
import com.xee.course.Fragments.WhatsNextFragment.WhatsNextFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.LessonData;
import com.xee.course.Utils.Database.DBHandler;

/**
 * Fragment displaying lesson
 */
public class LessonFragment extends BaseFragment {

    public static final String TAG = "lessonFragmentTag";

    private static int lessonId;

    public LessonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_lesson, container, false);

        if (getArguments() != null && getArguments().containsKey(Constants.KEY_LESSON_ID)) {
            lessonId = getArguments().getInt(Constants.KEY_LESSON_ID);
        }
        setHasOptionsMenu(true);

        //getting lesson's data
        final LessonData lessonData = DBHandler.getInstance(getContext()).getLessonDataByLessonId(lessonId);

        //set title
        String text = lessonData.getTitle();
        ((TextView) rootView.findViewById(R.id.tv_lesson_name)).setText(text);


        //loading from lesson's data lesson plan to web view
        //forming file path string
        String filePath = "file:///" + lessonData.getFilePasText();
        WebView webView = (WebView) rootView.findViewById(R.id.lesson_web_view);
        webView.loadUrl(filePath);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);


        //setting photo
        final ImageView photoHolder = (ImageView) rootView.findViewById(R.id.lesson_image_holder);

        //setting image to holder
        photoHolder.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    // Wait until layout to call Picasso
                    @Override
                    public void onGlobalLayout() {
                        // Ensure we call this only once
                        photoHolder.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);

                        Picasso.with(getContext())
                                .load(lessonData.getFilePasPhoto())
                                .resize(photoHolder.getMeasuredWidth(), photoHolder.getMeasuredHeight())
                                .centerInside()
                                .into(photoHolder);
                    }
                });

        photoHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), PhotoViewActivity.class);
                i.putExtra(Constants.KEY_LESSON_ID, lessonData.getId());
                startActivity(i);
            }
        });

        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lesson, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle lessonIdBundle;

        switch (item.getItemId()) {
            case android.R.id.home:
//                ((MainActivity) getActivity()).showFragment(LessonsListFragment.TAG);
                getActivity().onBackPressed();
                return true;

            case R.id.menu_lesson_add_note:
                lessonIdBundle = new Bundle();
                lessonIdBundle.putInt(Constants.KEY_LESSON_ID, lessonId);
                ((MainActivity) getActivity()).showFragment(NoteFragment.TAG, lessonIdBundle);
                return true;

            case R.id.menu_lesson_notes_list:
                lessonIdBundle = new Bundle();
                lessonIdBundle.putInt(Constants.KEY_LESSON_ID, lessonId);
                ((MainActivity) getActivity()).showFragment(NotesListFragment.TAG, lessonIdBundle);
                return true;

            case R.id.menu_lesson_plan:
                lessonIdBundle = new Bundle();
                lessonIdBundle.putInt(Constants.KEY_LESSON_ID, lessonId);
                ((MainActivity) getActivity()).showFragment(LessonPlanFragment.TAG, lessonIdBundle);
                break;

            case R.id.menu_lesson_whats_next:
                lessonIdBundle = new Bundle();
                lessonIdBundle.putInt(Constants.KEY_LESSON_ID, lessonId);
                ((MainActivity) getActivity()).showFragment(WhatsNextFragment.TAG, lessonIdBundle);
                break;
        }
        return false;
    }


    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
