package com.xee.course.Fragments.WhatsNextFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.xee.course.Fragments.BaseFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.LessonData;
import com.xee.course.Utils.Database.DBHandler;

/**
 * A simple {@link Fragment} subclass.
 */
public class WhatsNextFragment extends BaseFragment {
    public static final String TAG = "whatsNextFrTag";

    public WhatsNextFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_whats_next, container, false);

        setHasOptionsMenu(true);

        int lessonId;
        if (getArguments() != null && getArguments().containsKey(Constants.KEY_LESSON_ID)) {
            lessonId = getArguments().getInt(Constants.KEY_LESSON_ID);
        } else {
            throw new IllegalArgumentException("have to pass lesson ID to fragment");
        }

        //getting lesson's data
        LessonData lessonData = DBHandler.getInstance(getContext()).getLessonDataByLessonId(lessonId);

        //loading from lesson's data lesson plan to web view
        //forming file path string
        String filePath = "file:///" + lessonData.getFilePasWhatsNext();

        WebView webView = (WebView) rootView.findViewById(R.id.whats_next_web_view);
        webView.loadUrl(filePath);
//        webView.loadUrl("file:///android_asset/lessons/temp_lesson.html");
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        return rootView;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
