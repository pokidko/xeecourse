package com.xee.course.Fragments.LessonsListFragment;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xee.course.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LessonRVHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.lesson_title)
    TextView lessonTitle;

    @Bind(R.id.lesson_number)
    TextView lessonNumber;

    @Bind(R.id.lesson_list_item_holder_layout)
    LinearLayout layout;

    public LessonRVHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
