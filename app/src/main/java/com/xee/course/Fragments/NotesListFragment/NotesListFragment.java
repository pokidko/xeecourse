package com.xee.course.Fragments.NotesListFragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.Fragments.ConfirmDeletionDialogFragment;
import com.xee.course.Fragments.NoteFragment.NoteFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.Note;
import com.xee.course.Utils.Database.DBHandler;
import com.xee.course.Utils.Eventbus.MessageEvent;

import java.util.ArrayList;

/**
 * Displaying all notes
 */
public class NotesListFragment extends BaseFragment {
    public static final String TAG = "notesListFrTag";

    RecyclerView recyclerView;
    NoteRVAdapter rvAdapter;
    RecyclerView.LayoutManager rvLayoutManager;

    //handling on left swipe delete
    ItemTouchHelper touchHelper = new ItemTouchHelper(
            new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                    ItemTouchHelper.LEFT) {

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    if (direction == ItemTouchHelper.LEFT) {
                        ConfirmDeletionDialogFragment
                                .newInstance(rvAdapter.getData().get(viewHolder.getAdapterPosition()).getId())
                                .show(getFragmentManager(), "confirmDeletion");
                    }
                }
            });

    public NotesListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onMessageEvent(MessageEvent event) {
        switch (event.message) {
            //message from deletion dialog fragment
            case MENU_DELETION_CONFIRM:
                DBHandler dbHandler = DBHandler.getInstance(getContext());
                //getting note to delete
                Note note = dbHandler.getNoteById((int) event.object);

                //removing from db
                dbHandler.deleteNote(note);

                //update dataset in recycle view
                rvAdapter.setData(new ArrayList<>(dbHandler.getAllNotes()));
                rvAdapter.notifyDataSetChanged();

                //setting visibility of "no note" text
                setVisibilityOfDataViews((TextView) getActivity().findViewById(R.id.no_notes_in_list));
                break;

            //message from deletion dialog fragment
            case MENU_DELETION_CONFIRMATION_CANCEL:
                rvAdapter.notifyDataSetChanged();
                break;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notes_list, container, false);
        initRV(rootView);

        setHasOptionsMenu(true);

        return rootView;
    }

    private void initRV(View rootView) {
        ArrayList<Note> notes = new ArrayList<>();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.notes_recycle);
        rvLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(rvLayoutManager);
        rvAdapter = new NoteRVAdapter(notes, getContext());

        recyclerView.setAdapter(rvAdapter);

        //attaching swipe listener
        touchHelper.attachToRecyclerView(recyclerView);

        //getting note from DB
        if (getArguments() != null && getArguments().containsKey(Constants.KEY_LESSON_ID)) {
            //getting only notes related to lesson with passed id
            notes = new ArrayList<>(DBHandler.getInstance(getContext()).getNotesByLessonId(getArguments().getInt(Constants.KEY_LESSON_ID)));
        } else {
            notes = new ArrayList<>(DBHandler.getInstance(getContext()).getAllNotes());
        }

        rvAdapter.setData(notes);
        rvAdapter.notifyDataSetChanged();
        setVisibilityOfDataViews((TextView) rootView.findViewById(R.id.no_notes_in_list));
    }

    /**
     * show/hide recycle view and text view
     * depending on data array size
     */
    private void setVisibilityOfDataViews(TextView noNotesInArrayTextView) {
        //setting visibility of no lessons text view and RecycleView
        if (rvAdapter.getData().isEmpty()) {
            noNotesInArrayTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noNotesInArrayTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notes_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //todo: change to back fragment
//                ((MainActivity) getActivity()).showFragment(StudyingFragment.TAG);
                getActivity().onBackPressed();
                return true;

            case R.id.add_note:
                ((MainActivity) getActivity()).showFragment(NoteFragment.TAG);
                return true;
        }
        return false;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
