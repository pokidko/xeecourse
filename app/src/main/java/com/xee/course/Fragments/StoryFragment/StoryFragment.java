package com.xee.course.Fragments.StoryFragment;


import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.xee.course.Activities.SupportActivities.FragmentNavigationActivity;
import com.xee.course.Fragments.BaseFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.StoryData;
import com.xee.course.Utils.Database.DBHandler;

import java.io.IOException;

/**
 * Display story
 */
public class StoryFragment extends BaseFragment {
    public static final String TAG = "storyFragmentTag";
    StoryData storyData;

    //views
    ImageView playView;
    ImageView pauseView;
    ImageView stopView;
    ImageView volumeMaxView;
    ImageView volumeMuteView;

    MediaPlayer mediaPlayer = null;
//    AudioManager audioManager;

    //player support var
    boolean playerOnPause = false;

    View.OnClickListener playerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.player_play:
                    play();
                    break;

                case R.id.player_stop:
                    stop();
                    break;

                case R.id.player_pause:
                    pause();
                    break;

                case R.id.player_volume_mute:
                    unMute();
                    break;

                case R.id.player_volume_max:
                    mute();
                    break;

            }
        }
    };


    public StoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_story, container, false);

        setHasOptionsMenu(true);

        storyData = DBHandler.getInstance(getContext()).getStoryDataById(getArguments().getInt(Constants.KEY_STORY_ID));

        //set youtube link
        (rootView.findViewById(R.id.story_youtube_link)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String link = "https://www.youtube.com/watch?v=nYdCU1QQQro";
                String link = storyData.getYoutubeLink();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            }
        });

        //set title
//        String text = "История " + storyData.getId();
        String text = getStoryTitle(storyData.getId());
        ((TextView) rootView.findViewById(R.id.temp_tv_story_name)).setText(text);

        //setting photo
        final ImageView photoHolder = (ImageView) rootView.findViewById(R.id.story_image_holder);

        //setting image to holder
        photoHolder.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    // Wait until layout to call Picasso
                    @Override
                    public void onGlobalLayout() {
                        // Ensure we call this only once
                        photoHolder.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);

                        Picasso.with(getContext())
                                .load(storyData.getPhotoLink())
                                .resize(photoHolder.getMeasuredWidth(), photoHolder.getMeasuredHeight())
//                                .centerCrop()
                                .centerInside()
                                .into(photoHolder);
                    }
                });

//        Picasso.with(getContext())
//                .load(storyData.getPhotoLink())
//                .resize(photoHolder.getMeasuredWidth(), photoHolder.getMeasuredHeight())
//                .centerCrop()
//                .into(photoHolder);


        //adding audio player fragment
        //player's views
        playView = (ImageView) rootView.findViewById(R.id.player_play);
        pauseView = (ImageView) rootView.findViewById(R.id.player_pause);
        stopView = (ImageView) rootView.findViewById(R.id.player_stop);
        volumeMuteView = (ImageView) rootView.findViewById(R.id.player_volume_mute);
        volumeMaxView = (ImageView) rootView.findViewById(R.id.player_volume_max);

        playView.setOnClickListener(playerClickListener);
        pauseView.setOnClickListener(playerClickListener);
        stopView.setOnClickListener(playerClickListener);
        volumeMaxView.setOnClickListener(playerClickListener);
        volumeMuteView.setOnClickListener(playerClickListener);

//        audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        //TEXT - web view
        WebView webView = (WebView) rootView.findViewById(R.id.web_view);
        webView.loadUrl("file:///android_asset/" + storyData.getTextLink());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        return rootView;
    }

    /**
     * F***king hack
     * set title according to story ID
     * because i'm too lazy to rewrite DB
     */
    private String getStoryTitle(int id) {
        //todo: remove this hell sometime....maybe )
        String title = "История";
        switch (id) {
            case 1:
                title += " Малхаза";
                break;

            case 2:
                title += " Ани";
                break;

            case 3:
                title += " Даниила";
                break;

            case 4:
                title += " Дианы";
        }

        return title;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return false;
    }


    @Override
    public String getFragmentTag() {
        return TAG;
    }


    /**
     * Handle play click
     */
    public void play() {
        if (!playerOnPause) {
            mediaPlayer = new MediaPlayer();
            AssetFileDescriptor afd;
            try {
//                afd = getContext().getAssets().openFd("stories/temp_audio.mp3");
                afd = getContext().getAssets().openFd(storyData.getAudioFileLink());

                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

//            mediaPlayer = MediaPlayer.create(getContext(), R.raw.temp_audio);
            playerOnPause = false;
        }
        mediaPlayer.start();

        playView.setVisibility(View.GONE);
        pauseView.setVisibility(View.VISIBLE);
    }

    /**
     * un-Mute sound, change icon of sound
     */
    private void unMute() {
        if (mediaPlayer != null) {
            volumeMaxView.setVisibility(View.VISIBLE);
            volumeMuteView.setVisibility(View.GONE);
            mediaPlayer.setVolume(1, 1);
        }
    }

    /**
     * Mute sound, change icon of sound
     */
    private void mute() {
        if (mediaPlayer != null) {
            volumeMaxView.setVisibility(View.GONE);
            volumeMuteView.setVisibility(View.VISIBLE);
            mediaPlayer.setVolume(0, 0);
        }
    }

    /**
     * Handle stop click
     */
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            playerOnPause = false;

            playView.setVisibility(View.VISIBLE);
            pauseView.setVisibility(View.GONE);
        }
    }

    /**
     * Handle pause click
     */
    public void pause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            playerOnPause = true;

            playView.setVisibility(View.VISIBLE);
            pauseView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((FragmentNavigationActivity) getContext()).setBottomMenuItemChecked(getFragmentTag());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}
