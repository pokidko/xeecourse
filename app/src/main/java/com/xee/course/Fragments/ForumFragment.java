package com.xee.course.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.xee.course.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends BaseFragment {
    public static final String TAG = "forumFragTag";

    public static final String FORUM_URL = "http://forum.xee.life/";

    WebView webView;

    public ForumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forum, container, false);

        webView = (WebView) rootView.findViewById(R.id.forum_fragment_web_view);

        if (savedInstanceState != null) {
            Log.d(TAG, "onCreateView: loading from saved");
            webView.restoreState(savedInstanceState);
        } else {
            Log.d(TAG, "onCreateView: loading from url");
            webView.loadUrl(FORUM_URL);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //settings of web view
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setJavaScriptEnabled(true);

        //allow to load pages in app, not in browser
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        webView.saveState(outState);
        Log.d(TAG, "onSaveInstanceState: saved: ");
        super.onSaveInstanceState(outState);
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}

