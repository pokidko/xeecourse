package com.xee.course.Fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.xee.course.Activities.SupportActivities.FragmentNavigationActivity;
import com.xee.course.R;

public class MainFragment extends BaseFragment {
    public static final String TAG = "mainFragmentTag";

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

//        Picasso.with(getContext())
//                .load(R.mipmap.ic_launcher)
//                .into((ImageView) rootView.findViewById(R.id.image_holder_way));

        final ImageView imgView = (ImageView) rootView.findViewById(R.id.image_holder_way);
        imgView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
//                imgView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Log.d(TAG, "onGlobalLayout: h: " + imgView.getHeight() + " w: " + imgView.getWidth());
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((FragmentNavigationActivity) getContext()).setBottomMenuItemChecked(getFragmentTag());
        ((FragmentNavigationActivity) getContext()).hideAppBar();

    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
