package com.xee.course.Fragments.NotesListFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.NoteFragment.NoteFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.Note;
import com.xee.course.Utils.Database.DBHandler;

import java.util.ArrayList;

public class NoteRVAdapter extends RecyclerView.Adapter<NoteRVHolder> {

    Context context;
    LayoutInflater inflater;
    private ArrayList<Note> notes;

    public NoteRVAdapter(ArrayList<Note> notes, Context context) {
        this.notes = notes;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public NoteRVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = inflater.inflate(R.layout.note_holder, parent, false);
        return new NoteRVHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(NoteRVHolder holder, final int position) {
        Note note = notes.get(position);
        holder.noteTitle.setText(note.getTitleOfNote());
//        holder.noteText.setText(note.getCutTextOfNote());

        String lessonName = "";
        //if note connected to lesson
        if (note.getLessonId() != Note.NO_LESSON_NOTE) {
            //get lesson title by lesson id from note
            lessonName = DBHandler.getInstance(context).getLessonDataByLessonId(note.getLessonId()).getTitle();
        }
        holder.noteText.setText(lessonName);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle editNoteBundle = new Bundle();
                editNoteBundle.putInt(Constants.KEY_EDIT_NOTE_ID, notes.get(position).getId());
                ((MainActivity) context).showFragment(NoteFragment.TAG, editNoteBundle);
//                ((FragmentNavigationActivity)context).showFragmentAddToBackStack(FragmentNavigationActivity.NOTE_FRAGMENT, editNoteBundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public ArrayList<Note> getData() {
        return this.notes;
    }

    public void setData(ArrayList<Note> notes) {
        this.notes = notes;
    }
}
