package com.xee.course.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.xee.course.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends BaseFragment {
    public static final String TAG = "aboutUsTag";

    //    public static final String rdnTextFilePath = "file:///android_asset/about_us_text.html" ;
    //todo remove
    public static final String aboutUsFilePath = "file:///android_asset/lessons/temp_lesson.html";

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);

        WebView webView = (WebView) rootView.findViewById(R.id.about_us_fragment_web_view);
        webView.loadUrl(aboutUsFilePath);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        return rootView;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }
}
