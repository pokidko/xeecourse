package com.xee.course.Fragments.LessonsListFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xee.course.Activities.MainActivity;
import com.xee.course.Fragments.LessonFragment.LessonFragment;
import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.DataKeepingClasses.LessonData;

import java.util.ArrayList;

public class LessonsRVAdapter extends RecyclerView.Adapter<LessonRVHolder> {
    Context context;
    LayoutInflater inflater;
    private ArrayList<LessonData> lessons;

    public LessonsRVAdapter(ArrayList<LessonData> lessons, Context context) {
        this.lessons = lessons;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public LessonRVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = inflater.inflate(R.layout.lesson_rv_holder, parent, false);
        return new LessonRVHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(LessonRVHolder holder, int position) {
        final LessonData lessonData = lessons.get(position);
        holder.lessonTitle.setText(lessonData.getTitle());
        holder.lessonNumber.setText(context.getString(R.string.lesson)
                + " "
                + String.valueOf(lessonData.getLessonNumber()));

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((MainActivity) context).showFragment(LessonFragment.TAG);

                Bundle lessonIdBundle = new Bundle();
//                lessonIdBundle.putInt(Constants.KEY_LESSON_ID, lessonData.getLessonNumber());
                Log.d("debugTag", "onClick: lesson id " + lessonData.getId());
                lessonIdBundle.putInt(Constants.KEY_LESSON_ID, lessonData.getId());
                ((MainActivity) context).showFragment(LessonFragment.TAG, lessonIdBundle);

//                Bundle editNoteBundle = new Bundle();
//                editNoteBundle.putInt(Constants.KEY_EDIT_NOTE_ID, notes.get(position).getId());
//                ((FragmentNavigationActivity)context).showFragmentAddToBackStack(FragmentNavigationActivity.NOTE_FRAGMENT, editNoteBundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }

    public ArrayList<LessonData> getData() {
        return this.lessons;
    }

    public void setData(ArrayList<LessonData> lessonDatas) {
        this.lessons = lessonDatas;
    }

}
