package com.xee.course.Fragments.StoriesListFragment;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.xee.course.R;

public class StoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public String data;
    //    public TextView storyName;
    public ImageView photoImageView;
    public CardView holderLayout;

    public StoryViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        photoImageView = (ImageView) itemView.findViewById(R.id.story_holder_photo_image_view);
//        storyName = (TextView) itemView.findViewById(R.id.storyName);
        holderLayout = (CardView) itemView.findViewById(R.id.stories_story_holder);
    }

    @Override
    public void onClick(View view) {
        //todo
        Toast.makeText(view.getContext(), "Clicked " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}