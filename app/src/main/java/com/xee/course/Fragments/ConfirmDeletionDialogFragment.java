package com.xee.course.Fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.xee.course.R;
import com.xee.course.Utils.Constants;
import com.xee.course.Utils.Eventbus.MessageEvent;
import com.xee.course.Utils.Eventbus.Messages;

import org.greenrobot.eventbus.EventBus;

public class ConfirmDeletionDialogFragment extends DialogFragment {
    private Integer noteIdToDelete;

    /**
     * Getting instance of class with note Id
     */
    public static ConfirmDeletionDialogFragment newInstance(int noteId) {
        ConfirmDeletionDialogFragment fragment = new ConfirmDeletionDialogFragment();

        // input noteId as an argument.
        Bundle args = new Bundle();
        args.putInt(Constants.KEY_DELETE_NOTE_ID, noteId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null && getArguments().containsKey(Constants.KEY_DELETE_NOTE_ID)){
            noteIdToDelete = getArguments().getInt(Constants.KEY_DELETE_NOTE_ID);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.confirm_deletion_dialog_title)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(noteIdToDelete == null){
                            EventBus.getDefault().post(new MessageEvent(Messages.MENU_DELETION_CONFIRM, null));
                        } else {
                            EventBus.getDefault().post(new MessageEvent(Messages.MENU_DELETION_CONFIRM, noteIdToDelete));
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        EventBus.getDefault().post(new MessageEvent(Messages.MENU_DELETION_CONFIRMATION_CANCEL));
                    }
                });
        return builder.create();
    }
}
