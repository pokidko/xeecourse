package com.xee.course.Utils.Eventbus;

/**
 * Eventbus message class
 */
public class MessageEvent {
    public final Messages message;
    public final Object object;

    public MessageEvent(Messages message, Object object) {
        this.message = message;
        this.object = object;
    }

    public MessageEvent(Messages message) {
        this.message = message;
        this.object = null;
    }
}
