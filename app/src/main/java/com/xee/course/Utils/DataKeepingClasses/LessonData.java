package com.xee.course.Utils.DataKeepingClasses;

/**
 * Keeping lesson data from DB
 */
public class LessonData {
    private int id;
    private int lessonNumber;
    private String title;
    private int part;
    private String filePasText;
    private String filePasWhatsNext;
    private String filePasLessonPlan;
    private String youtubeLink;
    private String eeLink;
    private String filePasPhoto;

    public LessonData(int id, int lessonNumber, String title, int part, String filePasText,
                      String filePasWhatsNext, String filePasLessonPlan, String youtubeLink,
                      String eeLink, String filePasAudio) {
        this.id = id;
        this.lessonNumber = lessonNumber;
        this.title = title;
        this.part = part;
        this.filePasText = filePasText;
        this.filePasWhatsNext = filePasWhatsNext;
        this.filePasLessonPlan = filePasLessonPlan;
        this.youtubeLink = youtubeLink;
        this.eeLink = eeLink;
        this.filePasPhoto = filePasAudio;
    }

    public int getId() {
        return id;
    }

    public int getLessonNumber() {
        return lessonNumber;
    }

    public String getTitle() {
        return title;
    }

    public int getPart() {
        return part;
    }

    public String getFilePasText() {
        return filePasText;
    }

    public String getFilePasWhatsNext() {
        return filePasWhatsNext;
    }

    public String getFilePasLessonPlan() {
        return filePasLessonPlan;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public String getEeLink() {
        return eeLink;
    }

    public String getFilePasPhoto() {
        return filePasPhoto;
    }
}
