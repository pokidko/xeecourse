package com.xee.course.Utils.DataKeepingClasses;

import android.os.Bundle;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Keeping data for restoring fragment from shared preferences
 */
public class SavedFragmentData {
    String fragmentTag;
//    Bundle bundle;

    HashMap<String, Integer> bundleData;

    public SavedFragmentData() {
    }

    /**
     * Constructor
     *
     * @param bundle expect only int values in bundle
     */
    public SavedFragmentData(String fragmentTag, Bundle bundle) {
        this.fragmentTag = fragmentTag;
        setBundle(bundle);

//        this.bundle = bundle;
//        this.bundle = null;
    }

    /**
     * Getting instance of SavedFragmentData from JSON string
     */
    public static SavedFragmentData getDataFromJson(String json) {
        return new Gson().fromJson(json, SavedFragmentData.class);
    }

    public String getFragmentTag() {
        return fragmentTag;
    }

    public void setFragmentTag(String fragmentTag) {
        this.fragmentTag = fragmentTag;
    }

    public Bundle getBundle() {
        if (bundleData == null) {
            return null;
        } else {
            Bundle bundle = new Bundle();
            for (Map.Entry<String, Integer> stringIntegerEntry : bundleData.entrySet()) {
                bundle.putInt(stringIntegerEntry.getKey(), stringIntegerEntry.getValue());
            }

            return bundle;
        }
    }

    public void setBundle(Bundle bundle) {
        if (bundle == null) {
            bundleData = null;
        } else {
            bundleData = new HashMap<>();

            for (String key : bundle.keySet()) {
                bundleData.put(key, bundle.getInt(key));
            }
        }
    }

    /**
     * Getting json string from class instance
     */
    public String toJson() {
        return new Gson().toJson(this);
    }
}
