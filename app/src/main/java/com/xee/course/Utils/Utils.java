package com.xee.course.Utils;

import android.app.Activity;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import java.util.Arrays;
import java.util.LinkedList;

public class Utils {
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        if (activity.getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


    /**
     * Showing specified menu items (setting visibility true)
     */
    public static void showMenuItems(MenuItem... items) {
        LinkedList<MenuItem> itemsList = new LinkedList<>(Arrays.asList(items));
        for (MenuItem item : itemsList) {
            if (item != null) {
                item.setVisible(true);
            }
        }
    }

    /**
     * Hiding specified menu items (setting visibility false)
     */
    public static void hideMenuItems(MenuItem... items) {
        LinkedList<MenuItem> itemsList = new LinkedList<>(Arrays.asList(items));
        for (MenuItem item : itemsList) {
            if (item != null) {
                item.setVisible(false);
            }
        }
    }
}
