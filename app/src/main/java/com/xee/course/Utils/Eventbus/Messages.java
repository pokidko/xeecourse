package com.xee.course.Utils.Eventbus;

/**
 * Messages of EventBass
 * do not forget to write description of object sending with message
 */
public enum Messages {
    MENU_DELETION_CONFIRM, //can contain int value - note to delete, or not
    MENU_DELETION_CONFIRMATION_CANCEL
}
