package com.xee.course.Utils.DataKeepingClasses;

import com.xee.course.Utils.Constants;

import java.io.Serializable;

/**
 * Note is class for keeping notes
 */

public class Note implements Serializable {
    //int value to show that note is note related to any lesson
    public static final int NO_LESSON_NOTE = -2;
    public static int NO_ID = -1;
    private int id;
    private String titleOfNote;
    private String textOfNote;
    private int lessonId;


    private long creationDate;

    public Note(String textOfNote) {
        this.id = NO_ID;
        this.titleOfNote = cutTitleAccordingTOMaxLength(textOfNote);
        this.textOfNote = textOfNote;
        this.creationDate = getCurrentDate();
    }

    public Note(String titleOfNote, String textOfNote) {
        this.id = NO_ID;
        this.titleOfNote = titleOfNote;
        this.textOfNote = textOfNote;
        this.creationDate = getCurrentDate();
    }


    /**
     * Constructor for items from DB
     * cutting title after Constants.TITLE_MAX_LENGTH symbol
     */
    public Note(int id, String titleOfNote, String textOfNote, int lessonId, long creationDate) {
        this.id = id;
        this.titleOfNote = cutTitleAccordingTOMaxLength(titleOfNote);
        this.textOfNote = textOfNote;
        this.lessonId = lessonId;
        this.creationDate = creationDate;
    }

    /**
     * Cutting title of note according to max length
     *
     * @return new string value, cut string
     */
    public static String cutTitleAccordingTOMaxLength(String inputTitle) {
        if (inputTitle.length() <= Constants.TITLE_MAX_LENGTH) {
            return inputTitle;
        } else {
            //returning first (TITLE_MAX_LENGTH - 3) symbols plus "..."
            return ""
                    + inputTitle.substring(0, (Constants.TITLE_MAX_LENGTH - 3) - 1)
                    + "...";

        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitleOfNote() {
        return titleOfNote;
    }

    /**
     * setting title of note
     *
     * @param titleOfNote should be not longer then Constants.TITLE_MAX_LENGTH,
     *                    other symbols will be deleted
     */
    public void setTitleOfNote(String titleOfNote) {
        this.titleOfNote = cutTitleAccordingTOMaxLength(titleOfNote);
    }

    public String getTextOfNote() {
        return textOfNote;
    }

    public void setTextOfNote(String textOfNote) {
        this.textOfNote = textOfNote;

        if (titleOfNote.isEmpty()) {
            this.titleOfNote = cutTitleAccordingTOMaxLength(textOfNote);
        }
    }

    /**
     * Getting first Constants.NOTE_TEXT_PREVIEW_MAX_LENGTH symbols of text
     * need to make preview in recycle view item
     *
     * @return cut text of note
     */
    public String getCutTextOfNote() {
        if (textOfNote.length() <= Constants.NOTE_TEXT_PREVIEW_MAX_LENGTH) {
            return textOfNote;
        }

        return ""
                + textOfNote.substring(0, (Constants.NOTE_TEXT_PREVIEW_MAX_LENGTH - 3) - 1)
                + "...";
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    private long getCurrentDate() {
        //todo
        return 0;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    @Override
    public String toString() {
        return "{id: " + String.valueOf(id)
                + ", title: " + titleOfNote
                + ", text: " + textOfNote
                + ", lessonId: " + String.valueOf(lessonId)
                + ", creation date: " + String.valueOf(creationDate)
                + "}";
    }
}
