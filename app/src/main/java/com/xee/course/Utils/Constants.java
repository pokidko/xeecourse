package com.xee.course.Utils;

public class Constants {
    //max length of title in note
    public static final int TITLE_MAX_LENGTH = 30; //have to be the same number is in resources constants
    public static final int NOTE_TEXT_PREVIEW_MAX_LENGTH = 140;

    public static final String KEY_EDIT_NOTE_ID = "editNoteId"; //int
    public static final String KEY_CURRENT_NOTE_ID = "currentEditNoteId"; //int
    public static final String KEY_DELETE_NOTE_ID = "deleteId"; //int

    public static final String KEY_STORY_ID = "storyId";

    public static final String KEY_SAVED_FRAGMENT = "savedFragmentData";
    public static final String KEY_LESSON_ID = "keyLessonId";


    //from headpiece activity to main activity
    public static final String KEY_START_MAIN_FRAGMENT = "startMainFragment"; // bool

    //id of parts of studying
    public static final String KEY_PARTS_ID = "partId";
    public static final int INT_CONNECT_LIFE_PART = 0;
    public static final int INT_SHARE_LIFE = 1;
    public static final int INT_MULTIPLY_LIFE = 2;
}
