package com.xee.course.Utils.DataKeepingClasses;

/**
 * Keeps Story data in database
 */
public class StoryData {
    private int id;
    private String textLink;
    private String youtubeLink;
    private String audioFileLink;
    private String photoLink;

    public StoryData(int id, String textLink, String youtubeLink, String audioFileLink, String photoLink) {
        this.id = id;
        this.textLink = textLink;
        this.youtubeLink = youtubeLink;
        this.audioFileLink = audioFileLink;
        this.photoLink = photoLink;
    }

    public int getId() {
        return id;
    }

    public String getTextLink() {
        return textLink;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public String getAudioFileLink() {
        return audioFileLink;
    }

    public String getPhotoLink() {
        return photoLink;
    }
}
