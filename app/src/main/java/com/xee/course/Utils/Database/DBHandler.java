package com.xee.course.Utils.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.xee.course.Utils.DataKeepingClasses.LessonData;
import com.xee.course.Utils.DataKeepingClasses.Note;
import com.xee.course.Utils.DataKeepingClasses.StoryData;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Class that is responsible for interaction with DataBase
 */
public class DBHandler {
    private static DBHandler instance;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;


    /**
     * Constructor
     */
    private DBHandler(Context context) {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
    }

    /**
     * get instance of DBHandler
     */
    public static DBHandler getInstance(Context context) {
        if (instance == null) {
            instance = new DBHandler(context);
        }
        return instance;
    }

    /**
     * Adds specified Note to DB
     *
     * @param note Note instance need to be added to DB
     */
    public int addNoteToDB(Note note) {
        //get values for fields
        ContentValues value = getContentFormNote(note);

        //add to DB
        return (int) database.insert(DatabaseHelper.DATABASE_TABLE_NOTES, null, value);
    }


    /**
     * Updating note in DB
     * changing note with id of specified note in DB
     * with specified note
     *
     * @param note need to be updated
     */
    public void updateNote(Note note) {
        ContentValues value = getContentFormNote(note);
        database.update(DatabaseHelper.DATABASE_TABLE_NOTES,
                value,
                DatabaseHelper.NOTE_ID_COLUMN + "= ?", new String[]{Integer.toString(note.getId())});
    }

    /**
     * Deleting specified note from DB
     *
     * @param note to delete
     */
    public void deleteNote(Note note) {
        int noteId = note.getId();

        if (noteId == Note.NO_ID) {
            //todo
//            throw new IllegalArgumentException("passed note with wrong ID");
            return;
        }
        database.delete(DatabaseHelper.DATABASE_TABLE_NOTES,
                DatabaseHelper.NOTE_ID_COLUMN + "= ?",
                new String[]{Long.toString(noteId)});
    }

    /**
     * Getting note from DB by id
     *
     * @param noteId to get
     * @return note ID or null if no such note
     */
    public Note getNoteById(int noteId) {
        if (noteId == Note.NO_ID) {
            throw new IllegalArgumentException("passed note with wrong ID");
        }

        String query = "select * from "
                + DatabaseHelper.DATABASE_TABLE_NOTES
                + " where "
                + DatabaseHelper.NOTE_ID_COLUMN
                + " = "
                + noteId;

        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.NOTE_ID_COLUMN));
        String title = cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE_TITLE_COLUMN));
        String text = cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE_TEXT_COLUMN));
        int lessonId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.NOTE_RELATED_LESSON_ID));
        long creationDate = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.NOTE_CREATION_DATE_COLUMN));

        cursor.close();
        return new Note(id, title, text, lessonId, creationDate);
    }

    /**
     * Getting notes by lesson ID
     */
    public ArrayList<Note> getNotesByLessonId(int lessonId) {
        LinkedList<Note> notes = new LinkedList<>();

        String query = "select * from "
                + DatabaseHelper.DATABASE_TABLE_NOTES
                + " where "
                + DatabaseHelper.NOTE_RELATED_LESSON_ID
                + " = "
                + lessonId;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int noteId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.NOTE_ID_COLUMN));
            String titleOfNote = cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE_TITLE_COLUMN));
            String textOfNote = cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE_TEXT_COLUMN));
            long creationDate = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.NOTE_CREATION_DATE_COLUMN));

            notes.add(new Note(noteId, titleOfNote, textOfNote, lessonId, creationDate));
        }

        cursor.close();
        return new ArrayList<>(notes);
    }

    /**
     * Getting all notes from data base
     *
     * @return array of notes from DB
     */
    public LinkedList<Note> getAllNotes() {
        LinkedList<Note> notes = new LinkedList<>();

        Cursor cursor = database.query(DatabaseHelper.DATABASE_TABLE_NOTES,
                new String[]{DatabaseHelper.NOTE_ID_COLUMN, DatabaseHelper.NOTE_TEXT_COLUMN,
                        DatabaseHelper.NOTE_TITLE_COLUMN, DatabaseHelper.NOTE_RELATED_LESSON_ID, DatabaseHelper.NOTE_CREATION_DATE_COLUMN},
                null, null, null, null, null);

        while (cursor.moveToNext()) {
            int noteId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.NOTE_ID_COLUMN));
            String titleOfNote = cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE_TITLE_COLUMN));
            String textOfNote = cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE_TEXT_COLUMN));
            int lessonId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.NOTE_RELATED_LESSON_ID));
            long creationDate = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.NOTE_CREATION_DATE_COLUMN));

            notes.add(new Note(noteId, titleOfNote, textOfNote, lessonId, creationDate));
        }

        cursor.close();

        return notes;
    }


    /**
     * Forming Content to DB from Note
     * (preparing data to put in DB)
     *
     * @param note to get data from
     * @return ContentValues - data ready to put in DB
     */
    private ContentValues getContentFormNote(Note note) {
        ContentValues value = new ContentValues();

        value.put(DatabaseHelper.NOTE_TITLE_COLUMN, note.getTitleOfNote());
        value.put(DatabaseHelper.NOTE_TEXT_COLUMN, note.getTextOfNote());
        value.put(DatabaseHelper.NOTE_RELATED_LESSON_ID, note.getLessonId());
        value.put(DatabaseHelper.NOTE_CREATION_DATE_COLUMN, note.getCreationDate());

        return value;
    }


    /*
     * Lessons
     */
    public LessonData getLessonDataByLessonId(int lessonId) {
        String query = "select * from "
                + DatabaseHelper.DATABASE_TABLE_LESSONS
                + " where "
                + DatabaseHelper.LESSON_COLUMN_ID
                + " = "
                + lessonId;

        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        LessonData lessonData = getLessonDataFromCursor(cursor);
        cursor.close();

        return lessonData;
    }


    public ArrayList<LessonData> getLessonsListByPart(int partId) {
        LinkedList<LessonData> lessons = new LinkedList<>();

        String query = "select * from "
                + DatabaseHelper.DATABASE_TABLE_LESSONS
                + " where "
                + DatabaseHelper.LESSON_COLUMN_PART
                + " = "
                + partId;

        Cursor cursor = database.rawQuery(query, null);

        while (cursor.moveToNext()) {
            lessons.add(getLessonDataFromCursor(cursor));
        }

        cursor.close();
        return new ArrayList<>(lessons);

    }

    /**
     * Getting LessonData instance from cursor from DB
     */
    private LessonData getLessonDataFromCursor(Cursor cursor) {
        int lessonId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_ID));
        int lessonNumber = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_LESSON_NUMBER));
        String lessonTitle = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_TITLE));
        String filePassText = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_FILE_NAME_TEXT));
        String filePassWhatsNext = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_FILE_NAME_WHATS_NEXT));
        String filePassLessonPlan = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_FILE_NAME_LESSON_PLAN));
        String youtubeLink = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_YOUTUBE_LINK));
        String eeLink = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_EE_LINK));
        String filePassAudio = cursor.getString(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_FILE_NAME_PHOTO));
        int partId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.LESSON_COLUMN_PART));

        return new LessonData(lessonId, lessonNumber, lessonTitle,
                partId, filePassText, filePassWhatsNext,
                filePassLessonPlan, youtubeLink, eeLink, filePassAudio);
    }


    /*
     * Stories
     */

    /**
     * Getting storyData instance by specified id
     */
    public StoryData getStoryDataById(int id) {
        String query = "select * from "
                + DatabaseHelper.DATABASE_TABLE_STORIES
                + " where "
                + DatabaseHelper.STORIES_COLUMN_ID
                + " = "
                + id;

        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        StoryData storyData = getStoryDataFromCursor(cursor);
        cursor.close();
        return storyData;
    }

    /**
     * Getting story list
     */
    public ArrayList<StoryData> getStoriesList() {
        ArrayList<StoryData> stories = new ArrayList<>();

        Cursor cursor = database.query(DatabaseHelper.DATABASE_TABLE_STORIES,
                new String[]{DatabaseHelper.STORIES_COLUMN_ID, DatabaseHelper.STORIES_COLUMN_FILE_NAME_YOUTUBE_LINK,
                        DatabaseHelper.STORIES_COLUMN_FILE_NAME_TEXT, DatabaseHelper.STORIES_COLUMN_FILE_NAME_PHOTO,
                        DatabaseHelper.STORIES_COLUMN_FILE_NAME_AUDIO},
                null, null, null, null, null);

        while (cursor.moveToNext()) {
            stories.add(getStoryDataFromCursor(cursor));
        }

        cursor.close();
        Log.d("dbTag", "getStoriesList: size " + stories.size());
        return stories;
    }

    /**
     * Forming StoryData instance from cursor
     */
    private StoryData getStoryDataFromCursor(Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.STORIES_COLUMN_ID));
        String audioLink = cursor.getString(cursor.getColumnIndex(DatabaseHelper.STORIES_COLUMN_FILE_NAME_AUDIO));
        String youtueLink = cursor.getString(cursor.getColumnIndex(DatabaseHelper.STORIES_COLUMN_FILE_NAME_YOUTUBE_LINK));
        String textLink = cursor.getString(cursor.getColumnIndex(DatabaseHelper.STORIES_COLUMN_FILE_NAME_TEXT));
        String photoLink = cursor.getString(cursor.getColumnIndex(DatabaseHelper.STORIES_COLUMN_FILE_NAME_PHOTO));

        return new StoryData(id, textLink, youtueLink, audioLink, photoLink);
    }
}
