package com.xee.course.Utils.Database;


import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xee.course.Utils.DataKeepingClasses.LessonData;
import com.xee.course.Utils.DataKeepingClasses.StoryData;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    //names of table
    public static final String DATABASE_TABLE_NOTES = "notes";
    public static final String DATABASE_TABLE_LESSONS = "lessons";
    public static final String DATABASE_TABLE_STORIES = "stories";
    //notes - columns
    public static final String NOTE_ID_COLUMN = "noteID";
    public static final String NOTE_TITLE_COLUMN = "Title";
    public static final String NOTE_TEXT_COLUMN = "noteText";
    public static final String NOTE_RELATED_LESSON_ID = "relLessonId";
    public static final String NOTE_CREATION_DATE_COLUMN = "creationDate";
    //lessons - columns
    public static final String LESSON_COLUMN_ID = "lessonId";
    public static final String LESSON_COLUMN_LESSON_NUMBER = "lessonNumber";
    public static final String LESSON_COLUMN_TITLE = "lessonTitle";
    public static final String LESSON_COLUMN_PART = "lessonPart";
    public static final String LESSON_COLUMN_FILE_NAME_TEXT = "lessonFileText";
    public static final String LESSON_COLUMN_YOUTUBE_LINK = "lessonYoutube";
    public static final String LESSON_COLUMN_EE_LINK = "lessonEe";
    public static final String LESSON_COLUMN_FILE_NAME_WHATS_NEXT = "lessonFileWhats";
    public static final String LESSON_COLUMN_FILE_NAME_LESSON_PLAN = "lessonFilePlan";
    public static final String LESSON_COLUMN_FILE_NAME_PHOTO = "lessonFilePhoto";
    //stories - columns
    public static final String STORIES_COLUMN_ID = "storyId";
    public static final String STORIES_COLUMN_FILE_NAME_TEXT = "storyFileText";
    public static final String STORIES_COLUMN_FILE_NAME_YOUTUBE_LINK = "storyFileYoutube";
    public static final String STORIES_COLUMN_FILE_NAME_AUDIO = "storyFileAudio";
    public static final String STORIES_COLUMN_FILE_NAME_PHOTO = "storyFilePhoto";

    //name of DB
    private static final String DATABASE_NAME = "dataBase.db";
    //version of DB
    private static final int DATABASE_VERSION = 18;

    //scripts
    //Notes
    private static final String DATABASE_CREATE_SCRIPT_NOTES = "create table "
            + DATABASE_TABLE_NOTES + " ("
            + NOTE_ID_COLUMN + " integer primary key autoincrement,"
            + NOTE_TITLE_COLUMN + " text,"
            + NOTE_TEXT_COLUMN + " text,"
            + NOTE_RELATED_LESSON_ID + " integer,"
            + NOTE_CREATION_DATE_COLUMN + " long);";

    //lessons
    private static final String DATABASE_CREATE_SCRIPT_LESSONS = "create table "
            + DATABASE_TABLE_LESSONS + " ("
            + LESSON_COLUMN_ID + " integer primary key,"
            + LESSON_COLUMN_LESSON_NUMBER + " integer,"
            + LESSON_COLUMN_TITLE + " text,"
            + LESSON_COLUMN_PART + " integer,"
            + LESSON_COLUMN_FILE_NAME_TEXT + " text,"
            + LESSON_COLUMN_FILE_NAME_WHATS_NEXT + " text,"
            + LESSON_COLUMN_FILE_NAME_LESSON_PLAN + " text,"
            + LESSON_COLUMN_YOUTUBE_LINK + " text,"
            + LESSON_COLUMN_EE_LINK + " text,"
            + LESSON_COLUMN_FILE_NAME_PHOTO + " text);";

    //Stories
    private static final String DATABASE_CREATE_SCRIPT_STORIES = "create table "
            + DATABASE_TABLE_STORIES + " ("
            + STORIES_COLUMN_ID + " integer primary key,"
            + STORIES_COLUMN_FILE_NAME_TEXT + " text,"
            + STORIES_COLUMN_FILE_NAME_YOUTUBE_LINK + " text,"
            + STORIES_COLUMN_FILE_NAME_PHOTO + " text,"
            + STORIES_COLUMN_FILE_NAME_AUDIO + " text);";

    Context context;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * Creation of DB
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SCRIPT_NOTES);
        db.execSQL(DATABASE_CREATE_SCRIPT_LESSONS);
        db.execSQL(DATABASE_CREATE_SCRIPT_STORIES);
        initLessons(db);
        initStories(db);
    }


    /**
     * @param db         database
     * @param oldVersion of DB (id)
     * @param newVersion of DB (id)
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //delete existing table
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_NOTES);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_LESSONS);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_STORIES);

        // create new ones
        onCreate(db);
    }

    /**
     * Initializing stories data
     */
    private void initStories(SQLiteDatabase sqLiteDatabase) {
        final String INIT_STORIES_JSON_LINK = "stories/init_stories.json";

        Gson gson = new Gson();
        ArrayList<StoryData> stories = null;

        try {
            AssetManager assetManager = context.getAssets();
            InputStream ims = assetManager.open(INIT_STORIES_JSON_LINK);

            Reader reader = new InputStreamReader(ims);
            stories = gson.fromJson(reader,
                    new TypeToken<List<StoryData>>() {
                    }.getType());

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (stories != null) {
            ContentValues contentValues;
            for (StoryData story : stories) {
                contentValues = new ContentValues();

                contentValues.put(STORIES_COLUMN_ID, story.getId());
                contentValues.put(STORIES_COLUMN_FILE_NAME_TEXT, story.getTextLink());
                contentValues.put(STORIES_COLUMN_FILE_NAME_YOUTUBE_LINK, story.getYoutubeLink());
                contentValues.put(STORIES_COLUMN_FILE_NAME_PHOTO, story.getPhotoLink());
                contentValues.put(STORIES_COLUMN_FILE_NAME_AUDIO, story.getAudioFileLink());

                sqLiteDatabase.insert(DATABASE_TABLE_STORIES, null, contentValues);
            }
        }
    }


    /**
     * Initialization of lessons
     */
    private void initLessons(SQLiteDatabase sqLiteDatabase) {
        final String INIT_LESSONS_JSON_LINK = "lessons/init_lessons.json";

        Gson gson = new Gson();

        ArrayList<LessonData> lessons = null;

        try {
            AssetManager assetManager = context.getAssets();
            InputStream ims = assetManager.open(INIT_LESSONS_JSON_LINK);

            Reader reader = new InputStreamReader(ims);
            lessons = gson.fromJson(reader,
                    new TypeToken<List<LessonData>>() {
                    }.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (lessons != null) {
            ContentValues contentValues;
            for (LessonData lesson : lessons) {
                contentValues = new ContentValues();

                contentValues.put(LESSON_COLUMN_ID, lesson.getId());
                contentValues.put(LESSON_COLUMN_LESSON_NUMBER, lesson.getLessonNumber());
                contentValues.put(LESSON_COLUMN_TITLE, lesson.getTitle());
                contentValues.put(LESSON_COLUMN_PART, lesson.getPart());
                contentValues.put(LESSON_COLUMN_FILE_NAME_TEXT, lesson.getFilePasText());
                contentValues.put(LESSON_COLUMN_FILE_NAME_WHATS_NEXT, lesson.getFilePasWhatsNext());
                contentValues.put(LESSON_COLUMN_FILE_NAME_LESSON_PLAN, lesson.getFilePasLessonPlan());
                contentValues.put(LESSON_COLUMN_YOUTUBE_LINK, lesson.getYoutubeLink());
                contentValues.put(LESSON_COLUMN_EE_LINK, lesson.getEeLink());
                contentValues.put(LESSON_COLUMN_FILE_NAME_PHOTO, lesson.getFilePasPhoto());

                sqLiteDatabase.insert(DATABASE_TABLE_LESSONS, null, contentValues);
            }
        }
    }
}
